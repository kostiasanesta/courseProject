<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>DO IT!</title>
</head>
<body>
<c:choose>
    <c:when test="${pageContext.request.isUserInRole('admin')}">
        <button onclick="window.location.href='/mavenTestProject/private/exit'">logOut</button>
    </c:when>
    <c:when test="${pageContext.request.isUserInRole('user')}">
        <button onclick="window.location.href='/mavenTestProject/private/exit'">logOut</button>
    </c:when>
    <c:otherwise>
        <button onclick="window.location.href='/mavenTestProject/private'">login</button>
        <button onclick="window.location.href='/mavenTestProject/registration'">reg</button>
    </c:otherwise>
</c:choose>
<button onclick="window.location.href='/mavenTestProject/shop'">shop</button>
</body>


</html>