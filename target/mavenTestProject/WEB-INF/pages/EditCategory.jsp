<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:url var="editCategory" value="/private/categories"/>
<form:form action="${editCategory}" modelAttribute="category" method="post">
    <br><input hidden name="idCategory" value="${category.idCategory}">
    <br>Category Name <input name="catName" value="${category.nameCategory}">
    <br> Status <select size="1" name="status">
    <option selected value="${category.status}">
        <c:out value="${category.status}"/>
    </option>
    <option value="${!(category.status)}">
        <c:out value="${!(category.status)}"/>
    </option>
</select><br>
    <input type="submit" value="edit">
</form:form>
<button onclick="window.location.href='/mavenTestProject/private?action=showCategory'">Back</button>
</body>
</html>
