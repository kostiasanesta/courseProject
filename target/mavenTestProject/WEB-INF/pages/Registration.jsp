<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head><title>REGISTRATION</title></head>
<body>
<c:url var="saveAction" value="/registration/add"/>
<form:form action="${saveAction}" modelAttribute="newUser" method="post">
    Account name <form:input  path="nickname"/>
    <br> Name <form:input path="name"/>
    <br>Surname <form:input path="surname"/>
    <br>EMAIL <form:input path="email"/><br>
    <br>Password <form:input path="password"/>
    <br><br>Phone <form:input path="phone"/><br>
    <br>
    <form:button type="submit" value="reg">add</form:button></form:form>
<c:choose>
    <c:when test="${pageContext.request.isUserInRole('admin')}">
        <button onclick="window.location.href='/mavenTestProject/private'">Back</button>
    </c:when>
    <c:otherwise>
        <button onclick="window.location.href='/'">Back</button>
    </c:otherwise>
</c:choose>


</body>
</html>

