<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:forEach var="item" items="${itemsForSale}">
    <c:if test="${item.category.status}">
        <c:if test="${item.itemStatus}">
            <tr>
                <td><c:out value="${item.nameItem}"/></td>
                <td><
                    <button onclick="window.location.href='/mavenTestProject/private/bucket?action=toOrder&id=${item.idItem}'">
                        to order
                    </button>
                </td>
            </tr>
        </c:if>
    </c:if>
</c:forEach>
<br>
<button onclick="window.location.href='/mavenTestProject/private/bucket'">Back</button>
<br>
<button onclick="window.location.href='/mavenTestProject'">Back</button>
</body>
</html>
