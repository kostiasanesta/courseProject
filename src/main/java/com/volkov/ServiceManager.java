package com.volkov;


import com.volkov.services.*;
import com.volkov.services.interfaces.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("serviceManager")
public class ServiceManager {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private OrderItemService orderItemService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;

    public IService getItemService(){
        return itemService;
    }

    public IService getUserService(){
        return userService;
    }

    public IService getCategoryService(){
        return categoryService;
    }

    public IService getOrderItemService(){
        return orderItemService;
    }

    public IService getOrderService(){
        return orderService;
    }

}
