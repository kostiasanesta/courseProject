package com.volkov;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.volkov.beans.Category;
import com.volkov.beans.User;
import com.volkov.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.SecurityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProjectController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private OrderItemService orderItemService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistration(Model model) {
        model.addAttribute("newUser", new User());
        return "Registration";
    }

    @RequestMapping(value = "/registration/add", method = RequestMethod.POST)
    public String checkNewUserCreation(@ModelAttribute("newUser") User user) {
        if (userService.checkToAddUser(userService.makeUser(user))) {
            return "Private";
        }
        return "Registration";
    }

    @RequestMapping(value = "/private", method = RequestMethod.GET)
    public String showPrivateRoom(Model model) {
        return "Private";
    }

    @RequestMapping(value = "/private/orders", method = RequestMethod.GET)
    public String showOrders(Model model) {
        model.addAttribute("orders", orderService.getOrders());
        return "Orders";
    }

    @RequestMapping(value = "/private/users", method = RequestMethod.GET)
    public String showUsers(Model model) {
        model.addAttribute("users", userService.getUsers());
        return "Users";
    }

    @RequestMapping(value = "/private/items", method = RequestMethod.GET)
    public String showItems(Model model) {
        model.addAttribute("items", itemService.getItems());
        return "Items";
    }

    @RequestMapping(value = "/private/categories", method = RequestMethod.GET)
    public String showCategories(Model model) {
        model.addAttribute("categories", categoryService.getCategories());
        return "Categories";
    }

    @RequestMapping(value = "/private/categories/{idCat}/edit")
    public String editCategories(@PathVariable("idCat") int idCat, Model model) {
            model.addAttribute("category", categoryService.findCategoryById(idCat));
            return "EditCategory";
    }




}
