package com.volkov.beans;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_ORDER_ITEM")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.item",
                joinColumns = @JoinColumn(name = "ID_ITEM")),
        @AssociationOverride(name = "primaryKey.order",
                joinColumns = @JoinColumn(name = "ID_ORDER"))
})
public class OrderItem implements Serializable {
    private OrderItemID primaryKey = new OrderItemID();
    private int amount;

    @EmbeddedId
    public OrderItemID getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(OrderItemID primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Column(name = "AMOUNT")
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Transient
    public Order getOrder() {
        return primaryKey.getOrder();
    }

    public void setOrder(Order order) {
        primaryKey.setOrder(order);
    }

    @Transient
    public Item getItem() {
        return primaryKey.getItem();
    }

    public void setItem(Item item) {
        primaryKey.setItem(item);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderItem orderItem = (OrderItem) o;

        if (amount != orderItem.amount) return false;
        return primaryKey != null ? primaryKey.equals(orderItem.primaryKey) : orderItem.primaryKey == null;
    }

    @Override
    public int hashCode() {
        int result = primaryKey != null ? primaryKey.hashCode() : 0;
        result = 31 * result + amount;
        return result;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "primaryKey=" + primaryKey +
                ", amount=" + amount +
                '}';
    }
}
