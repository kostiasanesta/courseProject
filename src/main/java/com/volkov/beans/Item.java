package com.volkov.beans;

import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "T_ITEM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Item implements Serializable{
    private int idItem;
    private String nameItem;
    private Category category;
    private Boolean itemStatus;
    private List<OrderItem> orderItems;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ITEM")
    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }


    @Column(name = "NAME")
    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }


    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CATEGORY")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    @Column(name = "STATUS")
    public Boolean getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(Boolean itemStatus) {
        this.itemStatus = itemStatus;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryKey.item", cascade = CascadeType.ALL)
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (idItem != item.idItem) return false;
        if (nameItem != null ? !nameItem.equals(item.nameItem) : item.nameItem != null) return false;
        if (category != null ? !category.equals(item.category) : item.category != null) return false;
        if (itemStatus != null ? !itemStatus.equals(item.itemStatus) : item.itemStatus != null) return false;
        return orderItems != null ? orderItems.equals(item.orderItems) : item.orderItems == null;
    }

    @Override
    public int hashCode() {
        int result = idItem;
        result = 31 * result + (nameItem != null ? nameItem.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (itemStatus != null ? itemStatus.hashCode() : 0);
        result = 31 * result + (orderItems != null ? orderItems.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "idItem=" + idItem +
                ", nameItem='" + nameItem + '\'' +
                ", category=" + category +
                ", itemStatus=" + itemStatus +
                ", orderItems=" + orderItems +
                '}';
    }
}
