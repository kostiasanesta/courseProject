package com.volkov;


import org.hibernate.SessionFactory;

public interface IHibernate {
    SessionFactory getSessionFactory();
}
