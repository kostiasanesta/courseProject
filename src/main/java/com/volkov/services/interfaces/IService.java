package com.volkov.services.interfaces;


import com.volkov.beans.Item;

import java.util.List;

public interface IService {
   <T> boolean add(T val);
   <T> void delete(T val);
   <T> boolean edit(T val);
   <T> List<T> read();
}
