package com.volkov.services;


import com.volkov.beans.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import java.util.List;


@Service("userDetailService")
public class UserDetailService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.checkByName(s);
        List<GrantedAuthority> authorities = buildUserAutorities(user.getRole());
        return buildUserForAuthentification(user, authorities);
    }

    private org.springframework.security.core.userdetails.User buildUserForAuthentification(User user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getNickname(),user.getPassword(),authorities);
    }

    private List<GrantedAuthority> buildUserAutorities(String role) {
        List<GrantedAuthority> setAuth= new ArrayList<GrantedAuthority>();
        setAuth.add(new SimpleGrantedAuthority(role));
        return setAuth;
    }
}
