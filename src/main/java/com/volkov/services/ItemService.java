package com.volkov.services;


import com.volkov.beans.Item;
import com.volkov.dao.DAO;
import com.volkov.services.interfaces.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Transactional(readOnly = true)
@Component("itemService")
public class ItemService implements IService{

    @Autowired
    private DAO dao;


    private Item findItemById(int idItem) {
        for (Item it : dao.read(new Item())) {
            if (it.getIdItem() == idItem) {
                return it;
            }
        }
        return null;
    }

    private List<Item> getItems() {
        return dao.read(new Item());
    }





    @Override
    public <T> boolean add(T val) {
        Item item = (Item) val;
        Item it = findItemById(item.getIdItem());
        if(it==null){
            dao.createOrUpdate(item);
            return true;
        }
        return false;
    }

    @Override
    public <T> void delete(T val) {
        Item item = (Item) val;
        dao.delete(item);
    }

    @Override
    public <T> boolean edit(T val) {
        Item item = (Item) val;
        Item it = findItemById(item.getIdItem());
        if(it!=null) {
            dao.createOrUpdate(item);
            return true;
        }
        return false;
    }

    @Override
    public <T> List<T> read() {
        return (List<T>) getItems();
    }
}
