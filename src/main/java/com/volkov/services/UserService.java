package com.volkov.services;


import com.volkov.beans.User;
import com.volkov.dao.DAO;
import com.volkov.services.interfaces.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import java.sql.Date;
import java.util.List;

@Transactional(readOnly = true)
@Component("userService")
public class UserService implements IService{

    @Autowired
    private DAO dao;

    public User makeUser(User user){
        user.setDate(new Date(new java.util.Date().getTime()));
        user.setRole("user");
        user.setStatus(true);
        return user;
    }

    private List<User> getUsers(){
        return dao.read(new User());
    }

    @Transactional(readOnly = false)
    public boolean checkToAddUser(User user) {
        for (User oldUser: dao.read(new User())){
            if(oldUser.getNickname().equals(user.getNickname())){
                return false;
            }
        }
        dao.createOrUpdate(user);
        return true;
    }

    public User checkByName(String name) {
        User user1 = null;
        for (User user: dao.read(new User())){
            if (user.getNickname().equals(name)){
                return user;
            }
        }
        return user1;
    }


    public boolean checkByPassword(User user, String password) {
        if(user.getPassword().equals(password)){
            return true;
        }
        return false;
    }

    public User findUser(String name, String password) {
        User user = checkByName(name);
        if(user!=null){
            if(checkByPassword(user,password)){
                return user;
            }
        }
        return null;
    }

    public void deleteUserById(String indexUser){
        User curUser = checkByName(indexUser);
        if(curUser!=null) {
            dao.delete(curUser);
        }
    }

    public void updateUser(User user){
        User curUser = checkByName(user.getNickname());
        if(curUser!=null){
        dao.createOrUpdate(user);
        }
    }
    public User getUserForSession(String userID){
        User  curUser = checkByName(userID);
        if(curUser!=null){
            User  newUser = new User();
            newUser.setName(curUser.getName());
            newUser.setSurname(curUser.getSurname());
            newUser.setRole(curUser.getRole());
            return newUser;
        }
        return null;
    }

    @Override
    public <T> boolean add(T val) {
        return false;
    }

    @Override
    public <T> void delete(T val) {

    }

    @Override
    public <T> boolean edit(T val) {
        return false;
    }

    @Override
    public <T> List<T> read() {
        return (List<T>) getUsers();
    }
}
