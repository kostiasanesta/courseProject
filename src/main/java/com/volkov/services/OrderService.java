package com.volkov.services;


import com.volkov.beans.Order;
import com.volkov.dao.DAO;
import com.volkov.services.interfaces.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Transactional(readOnly = true)
@Component("orderService")
public class OrderService implements IService {

    @Autowired
    private DAO dao;


    public Order checkforAddOrder(Order order) {
        for (Order it : dao.read(new Order())) {
            if (it.getIdOrder() == order.getIdOrder()) {
                return it;
            }
        }
        dao.createOrUpdate(order);
        return order;
    }

    public Order findById(int id) {
        for (Order order : dao.read(new Order())) {
            if (order.getIdOrder() == id) {
                return order;
            }
        }
        return null;
    }

    public List<Order> getOrders() {
        return dao.read(new Order());
    }

    public boolean makeOrder(String date, String amount, String idItem) {
        return false;
    }

    public List<Order> getUserOrders(String userPrincipal) {
        List<Order> curOrders = getOrders();
        for (Order order : curOrders) {
            if (order.getUser().getNickname().equals(userPrincipal)) {
                curOrders.add(order);
            }
        }
        return curOrders;
    }

    @Override
    public <T> boolean add(T val) {
        return false;
    }

    @Override
    public <T> void delete(T val) {

    }

    @Override
    public <T> boolean edit(T val) {
        return false;
    }

    @Override
    public <T> List<T> read() {
        return (List<T>) getOrders();
    }
}
