package com.volkov.services;


import com.volkov.beans.OrderItem;
import com.volkov.dao.DAOImpl;
import com.volkov.dao.DAO;
import com.volkov.services.interfaces.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Transactional(readOnly = true)
@Component("orderItemService")
public class OrderItemService implements IService {

    @Autowired
    private DAO dao;

    public void makeOrderItem(){

    }

    private OrderItem checkforAddOrderItem(OrderItem orderItem) {
        for (OrderItem it : dao.read(new OrderItem())) {
            if (it.equals(orderItem)) {
                return it;
            }
        }
        dao.createOrUpdate(orderItem);
        return orderItem;
    }

    public List<OrderItem> getOrderItems() {
        return dao.read(new OrderItem());
    }

    @Override
    public <T> boolean add(T val) {
        return false;
    }

    @Override
    public <T> void delete(T val) {

    }

    @Override
    public <T> boolean edit(T val) {
        return false;
    }

    @Override
    public <T> List<T> read() {
        return null;
    }
}
