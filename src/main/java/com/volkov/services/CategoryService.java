package com.volkov.services;


import com.volkov.beans.Category;
import com.volkov.dao.DAO;
import com.volkov.services.interfaces.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
@Component("categoryService")
public class CategoryService implements IService {
    @Autowired
    private DAO dao;


    public void addCategory(Category category) {
        dao.createOrUpdate(category);
    }

    public Category findCategoryById(int id) {
        for (Category cat : getCategories()) {
            if (cat.getIdCategory() == id) {
                return cat;
            }
        }
        return null;
    }

    @Transactional(readOnly = false)
    public void editCategory(int idCat) {
        Category category = findCategoryById(idCat);
        if (category != null) {
            dao.createOrUpdate(category);
        }
    }

    public Category checkforAddCategory(Category category) {
        for (Category cat : dao.read(new Category())) {
            if (cat.equals(category)) {
                return cat;
            }
        }
        dao.createOrUpdate(category);
        return category;
    }

    public List<Category> getCategories() {
        return dao.read(new Category());
    }

    @Override
    public <T> boolean add(T val) {
        return false;
    }

    @Override
    public <T> void delete(T val) {

    }

    @Override
    public <T> boolean edit(T val) {
        return false;
    }

    @Override
    public <T> List<T> read() {
        return null;
    }
}
