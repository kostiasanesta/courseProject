package com.volkov;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.volkov.beans.Category;
import com.volkov.beans.Item;
import com.volkov.beans.Order;
import com.volkov.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private String adminPage = "AdminPage";
    private String usersPage = "Users";
    private String orderPage = "Orders";
    private String categoriesPage = "Categories";
    private String itemsPage = "Items";
    private String orderInfoPage = "OrderInfo";
    private String addItemPage = "AddItem";
    private String editItemPage = "ItemsEdit";
    private String addCategoryPage = "CreateCategory";

    @Autowired
    private ServiceManager serviceManager;


    @RequestMapping("/")
    public String showAdminPage(){
        return adminPage;
    }

    @RequestMapping("/users")
    public String showUsersList(Model model){
        model.addAttribute("users", serviceManager.getUserService().read());
        return usersPage;
    }

    @RequestMapping("/orders")
    public String showOrdersPage(Model model){
        model.addAttribute("orders", serviceManager.getOrderService().read() );
        return orderPage;
    }

    @RequestMapping("/categories")
    public String showCategoriesPage(Model model){
        model.addAttribute("categories", serviceManager.getCategoryService().read());
        return categoriesPage;
    }

    @RequestMapping("/items")
    public String showItemsPage(Model model){
        model.addAttribute("items", serviceManager.getItemService().read());
        return itemsPage;
    }

    @RequestMapping("orders/info")
    public String showOrderInfo(@RequestParam("order")Order order, Model model){
        model.addAttribute("order", order);
        return orderInfoPage;
    }

    @RequestMapping("orders/edit")
    public String showEditedOrders(@RequestParam("order") Order order){
        serviceManager.getOrderService().edit(order);
        return "redirect:orders";
    }

    @RequestMapping("items/add")
    public String addItemPage(Model model){
        model.addAttribute("item", new Item());
        return addItemPage;
    }

    @RequestMapping(value = "items/add", method = RequestMethod.POST)
    public String addAndRedirectToItems(@RequestParam("item")Item item){
        if(serviceManager.getItemService().add(item)){
            return "redirect:items";
        }
        return "redirect:items/add";
    }

    @RequestMapping("items/edit")
    public String editItemPage(@RequestParam("item")Item item, Model model){
        model.addAttribute("item", item);
        return editItemPage;
    }

    @RequestMapping(value = "items/edit", method = RequestMethod.POST)
    public String editAndRedirectToItems(@RequestParam("item")Item item){
        if(serviceManager.getItemService().edit(item)){
            return "redirect:items";
        }
        return "redirect:items/edit";
    }

    @RequestMapping("items/delete")
    public String deleteItemPage(@RequestParam("item")Item item){
        serviceManager.getItemService().delete(item);
        return "redirect:items";
    }

    @RequestMapping("categories/add")
    public String addCategoryPage(Model model){
        model.addAttribute("category", new Category());
        return addCategoryPage;
    }

    @RequestMapping(value = "categories/add", method = RequestMethod.POST)
    public String addCategoryAndRedirect(@RequestParam("category")Category category){
        if(serviceManager.getCategoryService().add(category)){
            return "redirect:categories";
        }
        return "redirect:categories/add";
    }

    @RequestMapping("categories/edit")
    public String editCategoryPage(@RequestParam("category")Category category){
        serviceManager.getCategoryService().edit(category);
        return "redirect:categories";
    }





}
