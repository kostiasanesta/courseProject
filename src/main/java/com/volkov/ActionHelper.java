package com.volkov;


import javax.servlet.http.HttpServletRequest;

public class ActionHelper {

    public static boolean isCurAction(HttpServletRequest req, String actionName){
        if(req.getParameter("action")!=null && req.getParameter("action").equals(actionName)){
            return true;
        }
        return false;
    }

}
