package com.volkov.servlets;


import com.volkov.ConnectHelper;
import com.volkov.beans.User;
import com.volkov.services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Registration extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/WEB-INF/Registration.jsp");
        requestDispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        RequestDispatcher requestDispatcher;
        ServletContext context = getServletContext();
        PrintWriter out = resp.getWriter();
        String accName = req.getParameter("id");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String email = req.getParameter("email");
        String pass1 = req.getParameter("password");
        String pass2 = req.getParameter("confirm");
        String phone = req.getParameter("phone");

//        if (pass1.equals(pass2)){
//        if(accName!="" && name!="" && surname!="" && email!="" && pass1!=""){

//            User user = UserService.getInstance().makeUser(accName,name,surname,email,pass1,phone);
//            if(UserService.getInstance().checkToAddUser(user)){
//                if(req.getSession().getAttribute("user")==null) {
//                    req.getSession().setAttribute("user", user);
//                }
//                resp.sendRedirect("/mavenTestProject/private/?action="+user);
//            }else {
//                resp.sendRedirect("/mavenTestProject/private");
//            }
//        }
//    }else {
//            resp.sendRedirect("/mavenTestProject/registration");
//        }
    }
}
