package com.volkov;


import org.apache.log4j.Logger;

public class LoggerHelper {


    public static Logger getLogger(Object obj){
        return Logger.getLogger(obj.getClass());
    }
}
