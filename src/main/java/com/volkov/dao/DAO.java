package com.volkov.dao;


import java.util.List;

public interface DAO {
    <T> void createOrUpdate(T val);
    <T> List<T> read(T val);
    <T> void delete(T val);

}
