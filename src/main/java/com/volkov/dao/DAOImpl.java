package com.volkov.dao;


import com.volkov.LoggerHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Selection;
import java.util.List;
@Component("dao")
@Scope("prototype")
public class DAOImpl implements DAO {

    @Autowired
    private SessionFactory sessionFactory;

    public <T> void createOrUpdate(T val) {
        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(val);
//        transaction.commit();
        session.close();
        LoggerHelper.getLogger(this).info("cr or up HELLOOOO "+val.getClass().toString());
    }

    public <T> List<T> read(T val) {
        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = (CriteriaQuery<T>) criteriaBuilder.createQuery(val.getClass());
        criteriaQuery.select((Selection<? extends T>) criteriaQuery.from(val.getClass()));
        List<T> list = session.createQuery(criteriaQuery).getResultList();
//        transaction.commit();
        session.close();
        LoggerHelper.getLogger(this).info("read HELLOOOO "+val.getClass().toString());
        return list;
    }

    public <T> void delete(T val) {
        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
        session.delete(val);
//        transaction.commit();
        session.close();
        LoggerHelper.getLogger(this).info("delete HELLOOOO "+val.getClass().toString());
    }


}
