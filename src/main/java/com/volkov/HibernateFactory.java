package com.volkov;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;


public class HibernateFactory implements IHibernate{

    private static SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory(){

        return new Configuration().configure().buildSessionFactory();
    }

    @Override
    public SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public void shutdown(){
        getSessionFactory().close();
    }

}
