package com.volkov;



import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;




public class ConnectHelper {
    private Context context;
    private boolean exist;
    private DataSource dataSource;
    private static volatile ConnectHelper instance = null;

    private ConnectHelper(){
        exist=true;
    }
    public static ConnectHelper getInstance(){
        if(instance==null){
            synchronized (ConnectHelper.class){
                if (instance==null){
                    instance = new ConnectHelper();
                }
            }
        }
        return instance;
    }



    public Connection getConnection() throws NamingException, SQLException {
       if (exist) {
           context = new InitialContext();
           dataSource = (DataSource) context.lookup("java:comp/env/jdbc/test");
           exist=false;
       }
       return dataSource.getConnection();
    }


}
