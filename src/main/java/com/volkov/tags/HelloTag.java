package com.volkov.tags;


import com.volkov.beans.User;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class HelloTag extends SimpleTagSupport {
    private String key;

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public void doTag() throws JspException, IOException {
       User user = (User) getJspContext().findAttribute(key);
        if(user!=null){
            JspWriter js = getJspContext().getOut();
            js.println("PRIVED JSP CAPITAN "+user.getName());
        }
    }
}
