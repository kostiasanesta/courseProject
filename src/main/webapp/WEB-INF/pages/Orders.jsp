<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table width="100%" border="2" cellpadding="4">
    <button onclick="window.location.href='/mavenTestProject/private/orders'">add order</button>
    <tr>
        <td>ID_ORDER</td>
        <td>DATE</td>
        <%--<td>ID_ITEM</td>--%>
        <%--<td>NAME_CATEGORY</td>--%>
        <%--<td>STATUS</td>--%>
        <%--<td>ITEM_NAME</td>--%>
        <%--<td>AMOUNT</td>--%>
        <td>USER NAME</td>
        <td>USER SURNAME</td>
        <td>STATUS</td>
    </tr>
    <c:forEach var="order" items="${orders}">
        <tr>
        <td><c:out value="${order.idOrder}"/></td>
        <td><c:out value="${order.date}"/></td>
        <td><c:out value="${order.user}"/></td>
        <td><c:out value="${order.user}"/></td>
        <td><c:out value="${order.status}"/></td>
        <c:forEach var="orderItem" items="${order.orderItems}">
                <td><c:out value="${orderItem.item.nameItem}"/></td>
                <td><c:out value="${orderItem.amount}"/></td>
                <tr/>
        </c:forEach>
        </tr>
    </c:forEach>
</table>
<button onclick="window.location.href='/mavenTestProject/private'">Back</button>
<%--<button onclick="window.location.href='/mavenTestProject/private/orders?action=makeOrder'">Show Users</button>--%>
</body>
</html>
