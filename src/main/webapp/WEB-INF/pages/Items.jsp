<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <table width="100%" border="2" cellpadding="4">
        <tr>
            <td>NAME</td>
            <td>CATEGORY</td>
            <td>STATUS</td>
        </tr>
        <c:forEach var="item" items="${items}">
            <tr>
                <td><c:out value="${item.nameItem}"/></td>
                <td><c:out value="${item.category.nameCategory}"/></td>
                <td><c:out value="${item.itemStatus}"/></td>
                <td><button onclick="window.location.href='/mavenTestProject/private?action=edit&id=${item.idItem}'">Edit</button></td>
                <td><button onclick="window.location.href='/mavenTestProject/private?action=delete&id=${item.idItem}'">Delete</button></td>
            </tr>
        </c:forEach>
    </table>
    <c:if test="${pageContext.request.isUserInRole('admin')}">
    <button onclick="window.location.href='/mavenTestProject/private?action=addItem'">Add Item</button>
    </c:if>
        <button onclick="window.location.href='/mavenTestProject/private'">Back</button>
</body>
</html>
