<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:out value="Hello!"/>
</head>
<body>
<table width="100%" border="2" cellpadding="4">
<c:forEach var="user" items="${users}">
    <tr>
        <td><c:out value="${user.name}"/></td>
        <td><c:out value="${user.surname}"/></td>
        <td><c:out value="${user.email}"/></td>
        <td><c:out value="${user.phone}"/></td>
        <td><c:out value="${user.date}"/></td>
        <td><c:out value="${user.role}"/></td>
        <td><c:out value="${user.status}"/></td>
        <td><button onclick="window.location.href='/mavenTestProject/private/users/edit?action=edit&id=${user.nickname}'">Edit</button></td>
        <td><button onclick="window.location.href='/mavenTestProject/private/users/user?action=delete&id=${user.nickname}'">Delete</button></td>
    </tr>
</c:forEach>
    <button onclick="window.location.href='/mavenTestProject/private/registration'">add</button>
</table>
<button onclick="window.location.href='/mavenTestProject/private'">Back</button>
</body>
</html>
