<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="private" method="post">
    <br><input hidden name="idItem" value="${curItem.idItem}">
    <br>Item name <input name="name" value="${curItem.nameItem}">
    <br>Category <select size="1" name="category">
    <c:forEach var="category" items="${categoryList}">
        <c:choose>
            <c:when test="${category.idCategory==curItem.category.idCategory}">
                <option selected value="${curItem.category.idCategory}>">
                    <c:out value="${curItem.category.nameCategory}"/>
                </option>
            </c:when>
            <c:otherwise>
                <option value="${category.idCategory}">
                    <c:out value="${category.nameCategory}"/>
                </option>
            </c:otherwise>
        </c:choose>
    </c:forEach>

</select>
    <br> Status <select size="1" name="status">
    <option selected value="${curItem.itemStatus}">
        <c:out value="${curItem.itemStatus}"/>
    </option>
    <option value="${!(curItem.itemStatus)}">
        <c:out value="${!(curItem.itemStatus)}"/>
    </option>
</select><br>
    <br>
    <input type="submit" value="edit"></form>
<button onclick="window.location.href='/mavenTestProject/private?action=showItem'">Back</button>
</body>

</html>
