<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table width="100%" border="2" cellpadding="4">
    <tr>
        <td>NAME</td>
        <td>STATUS</td>
    </tr>
    <c:forEach var="category" items="${categories}">
        <tr>
            <td><c:out value="${category.nameCategory}"/></td>
            <td><c:out value="${category.status}"/></td>
            <td>
                <button onclick="window.location.href='/mavenTestProject/private/categories/${category.idCategory}/edit'">
                    Edit
                </button>
            </td>
            <td>
                <button onclick="window.location.href='/mavenTestProject/private/categories/${category.idCategory}/delete'">
                    Delete
                </button>
            </td>
        </tr>
    </c:forEach>
</table>

<br>
<button onclick="window.location.href='/mavenTestProject/private?action=createCat'">Create category</button>

<br>
<button onclick="window.location.href='/mavenTestProject/private'">Back</button>
</body>
</html>
