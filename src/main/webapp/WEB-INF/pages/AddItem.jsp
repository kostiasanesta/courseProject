<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="private?action=create" method="post">
    ITEM NAME<input name="itemName"><br>
    CATEGORY <select size="1" name="category">
    <option selected value="${null}">choose category</option>
    <c:forEach var="category" items="${categories}">
        <option value="${category.idCategory}">
            <c:out value="${category.nameCategory}"/>
        </option>
    </c:forEach>
</select><br>
    STATUS<select size="1" name="status">
    <option selected value="${true}">true</option>
    <option value="${false}">false</option>
</select><br>
    <input type="submit" value="add">
</form>
<button onclick="window.location.href='/mavenTestProject/private?action=showItem'">Back</button>
</body>
</html>
